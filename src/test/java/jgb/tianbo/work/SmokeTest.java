package jgb.tianbo.work;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class SmokeTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void should_return_status_code_200_ok_when_request_endpoint_of_health() {
        ResultActions result;
        try {
            result = mockMvc.perform(get("/actuator/health"));
            result.andExpect(status().is(200));
            result.andExpect(jsonPath("$.status").value("UP"));
        } catch (Exception ignored) {
        }
    }

    @Test
    public void should_1_plus_2_equals_to_3() {
        final double sum = 0.1 + 0.2;
        System.out.printf("0.1 + 0.2 = %s, do you know why?%n", sum);
        assertEquals("0.30000000000000004", Double.toString(sum));
    }
}
