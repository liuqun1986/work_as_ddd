CREATE TABLE IF NOT EXISTS `book_info` (
  `id` int NOT NULL AUTO_INCREMENT,
  `isbn_code` varchar(15) DEFAULT '',
  `title` varchar(1024) DEFAULT '',
  `author` varchar(128) DEFAULT '',
  `publisher` varchar(128) DEFAULT '',
  `pages` int DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;
