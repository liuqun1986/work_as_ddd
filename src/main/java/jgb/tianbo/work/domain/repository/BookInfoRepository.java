package jgb.tianbo.work.domain.repository;

import jgb.tianbo.work.domain.entity.BookInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookInfoRepository extends JpaRepository<BookInfo, Long> {
}
